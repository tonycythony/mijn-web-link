my_link = '117 105 105 109 110 39 50 50 122 116 \
		 105 117 104 127 51 126 114 112 50 92 115 \
		  105 114 115 116 116 116'


# encoder
xor = 29

def my_xor(a, xor):
	return ord(a) ^ xor

d = [my_xor(i,xor) for i in my_link]

mijn_link = ' '.join(str(i) for i in d)


# decoder for list
e = ''.join(str(chr(i ^ xor)) for i in d)


# decoder for string
link= ''.join(chr(i ^ xor) for i in map(int, my_link.split()))


# print(d, '\n', mijn_link, '\n', e)
print(link)
